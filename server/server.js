// // All Parties -- server
Meteor.publish("directory", function () {
  return Meteor.users.find({ _id: this.userId }, {fields: {emails: 1, profile: 1}});
}); 

Meteor.publish("voters", function () {
  return Voters.find({});
}); 


Meteor.headly.config({tagsForRequest: function(req) {
		var url = __meteor_bootstrap__.require('url'); 
		var host = 'http://' + req.headers.host;
		var reqUrl =  host + req.url ;
		var image = host + '/logo.jpg';
		var title = 'Vote for Pakistan in Elections 2013';
		var parsed = url.parse( req.url, true );

		if( parsed.query.party ){
			image = host+ '/' + parsed.query.party + '-og.jpg';
			title = parsed.query.party;
		}

		var fbAppId = '<meta property="fb:app_id" content="370064349767636" />\n';
		var ogType = '<meta property="og:type" content="voteforpk:party" />\n';
		var ogDesc = '<meta property="og:description" content="Click to Pledge Vote for your Party in 2013 Pakistan Elections and also see other Pakistanis\' Votes">';
		var ogTitle =  '<meta property="og:title" content="' + title + '" />\n'; 
		var ogImage =  '<meta property="og:image" content="' + image + '" />\n'; 
		var ogUrl = '<meta property="og:url" content="' + reqUrl + '" />\n'; 
		
		return fbAppId + ogType + ogDesc + ogTitle + ogImage + ogUrl;
	}
});