// All Voters -- data model
// Loaded both on Server and Client

/*
	Each Voter is represented by a document in in Voters Collection
		owner: user id
		lat: x
		lng: y
		party: selected part
 */

Voters = new Meteor.Collection( "voters" );

Voters.allow({
  insert: function (userId, voter) {
    return false; // no cowboy inserts -- use createParty method
  }

  // update: function (userId, voters, fields, modifier) {
  //   return _.all(voters, function (voter) {
  //     if (userId !== voter.owner)
  //       return false; // not the owner

  //     var allowed = ["party", "lat", "lng"];
  //     if (_.difference(fields, allowed).length)
  //       return false; // tried to write to forbidden field

  //     // A good improvement would be to validate the type of the new
  //     // value of the field (and if a string, the length.) In the
  //     // future Meteor will have a schema system to makes that easier.
  //     return true;
  //   });
  // },

  // remove: function(){
  // 	return true;
  // }
});

Meteor.methods({
	createVoter: function(options){	
		options = options || {};

    if( Voters.findOne({ owner: this.userId }) ) 
      throw new Meteor.Error(403, "You have already Voted");

		if (! (typeof options.party === "string" && options.party.length &&
		       typeof options.lat === "number" && // && options.lat >= 0 && options.lat <= 1 
		       typeof options.lng === "number" )) // && options.lng >= 0 && options.lng <= 1
		  throw new Meteor.Error(400, "Required parameter missing");

		if (options.party.length > 10)
		  throw new Meteor.Error(413, "party too long");
		if (! this.userId)
      throw new Meteor.Error(403, "You must be logged in");

    return Voters.insert({
      owner: this.userId,
      loc: { lat: options.lat, lng: options.lng},
      party: options.party
    });
	}
})