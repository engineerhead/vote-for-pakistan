// Application Flow:
//   x Load Map with Markers
//   x If user is Logged in and has Voted
//   x then Load User Marker and Other Markers
//   x else Authenticate User
//   x   If User is Authenticated
//   x   Then Ask for Which Party to Vote
//   #     Get Location of the User from FB and Map it [ Not possible due to Very Low API Limits]
//   x     Post OG Action on Facebook
//   x     Save Vote in DB with Owner for Current User
//   #     Send Requests through Facebook [Canvas App would be needed and it is against FB's TOS]
//   
// TODOs if more people start using the app
//   - Limit Results on the Query
//   - Limit Results within Range
//   # Set Map Bounds


// Subscribe to User Information
Meteor.subscribe("directory");

// Subscribe to Voters Table
// On Subscription, Initialize Session Variables by Creating 
// a Reactive Context using Meteor.autorun(  )
Meteor.subscribe("voters", function(){

  Meteor.autorun(function () {
    var uid = Meteor.userId();
    var voter = Voters.findOne({owner: uid})
    
    if (voter){
      Session.set( "voter",  voter._id);
      Session.set( "selectedParty", voter.party );
      Session.set( "coordinates", { lat: voter.loc.lat, lng: voter.loc.lng } );
      Session.set( "showPartyDialog", false );
    }
    else{
      Session.set( "showPartyDialog", true );
      Session.set( "voter",  null);
      Session.set( "selectedParty", "None" );
      Session.set( "coordinates", null ); 
    }


  });

});

/**
 * Global State Holder for the App
 * @type {Object}
 */
var myApp = {

  fbAppId: '370064349767636',

  gaId: 'UA-38465133-1',

  fbNamespace : 'voteforpk',

  mapId: '#map_canvas',

  renderedMarkers : [],

  tweetMsg: ['I pledged My Vote for ', ' in Pak Elections 2013. Pledge your Vote too at'],

  Init: function(){
    this.gaInit();
    this.fbInit();
    this.coordsInit();
  },

  /**
   * Initilizes Facebook SDK
   * @return {[type]} [description]
   */
  fbInit : function(){
    FB.init({
      appId      :  this.fbAppId, // App ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
    });
  },

  /**
   * Initializes Google Analytics
   * @return {[type]} [description]
   */
  gaInit: function(){
    window._gaq = window._gaq || [];
    _gaq.push(['_setAccount', this.gaId ]);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  },

  /**
   * Set User's Coordinates from Session or from IPDBInfo Service
   * @return {[type]} [description]
   */
  coordsInit: function(){
    var coords = Session.get( "coordinates" ),
        initLat = 30,
        initLng = 69;
    
    if( coords ){
      initLat = coords.lat,
      initLng = coords.lng;
      myApp.mapInit( initLat, initLng );
    }
    else{
      var visitorGeolocation = new geolocate(false, true);
      var callback = function(){
                      initLat = visitorGeolocation.getField('latitude'),
                      initLng = visitorGeolocation.getField('longitude');

                      myApp.mapInit( initLat, initLng );
                    };
      visitorGeolocation.checkcookie(callback);

    }

  },

  /**
   * Initializez Google Maps
   * @return {[type]} [description]
   */
  mapInit : function( initLat, initLng ){
    
    myApp.map = new GMaps({
      div: this.mapId,
      zoom: 9,
      lat: initLat,
      lng: initLng,
      disableDoubleClickZoom: true,
      markerClusterer: function(map) {
        return new MarkerClusterer(map);
      },
      dblclick: function(e) {
        
        // If Already a Voter or there is No Selected Party Return
        var party = Session.get( "selectedParty" );
        var voter = Session.get( "voter" );
        if( voter || party === "None" )
          return false;

        // Get Lat Lng and Image for Selected Party
        var lat = e.latLng.lat(  );
        var lng = e.latLng.lng(  ) ;
        var image = party + "-selected.png" ;
        
        // If there is already a Marker for User Set it NUll
        if( myApp.myMarker ) {
          myApp.map.markerClusterer.removeMarker(myApp.myMarker, false);
        }

        // Add Marker on The Location
        myApp.myMarker = myApp.map.addMarker({
          lat: lat, 
          lng: lng,
          icon: image
        });

        // Set Coordinates for Current Voter
        Session.set( "coordinates", { lat: lat, lng: lng });

      }
    });
  },

  updateTweetButton: function () {
    var message = this.tweetMsg[0] + Session.get( "selectedParty" ) + this.tweetMsg[1];
    var tweetButton = document.getElementById('tweet-button');
    tweetButton.src = tweetButton.src.replace(/&text=[^&]+/, "&text=" + encodeURIComponent(message) );
  }
}

/**
 * On Startup
 *   Call To App Init
 *   Subscribe to User Login and Logout to Render Updated Markers
 * @return {[type]}
 */
Meteor.startup(function(){

  Session.set( "showErrorDialog", false );

  // Bootstrap FB,GA,GMaps
  myApp.Init();

  // Subscribe to User Login and Logout
  Meteor.autosubscribe(function(){

    var uid = Meteor.userId();
    
    // On User Session Change, stop Query myApp.Observer if Any
    if( myApp.observer ){
      myApp.observer.stop(  );
      myApp.observer = null;
    }

    // If myApp.Map is Rendered it means that Current User has
    // changed so Clear Map Markers for Rerendering
    if( myApp.map ){
      // Clear Markers and Empty Rendered List
      myApp.map.markerClusterer.clearMarkers();
      myApp.renderedMarkers = [];
    }

    // If User and also a Voter, Draw Marker for Current User
    if( uid ){
      var voter = Session.get( "voter" );
      if( voter ){
        // Add Marker for Current User
        var coords = Session.get( "coordinates" );
        myApp.myMarker = myApp.map.addMarker({
          lat: coords.lat,
          lng: coords.lng,
          icon: Session.get( "selectedParty" ) + "-selected.png"
        });

        // Set Map Center, and Increase Zoom
        myApp.map.setCenter( coords.lat, coords.lng );
        myApp.map.setZoom( 10 );
      }
    }

    // myApp.Observer Query for Items where Current User is Not the Owner
    // On Addition of Items, Check if New Voter has been rendered aleady
    // If not already rendered, Add Marker and Push to an Array of Rendered Voters
    myApp.observer  = Voters.find({ owner: { $ne: uid } }).observe({
        added: function(voter,index){
          if( _.indexOf( myApp.renderedMarkers, voter._id ) === -1 ){
            myApp.renderedMarkers.push(voter._id);
            myApp.map.addMarker({
              lat: voter.loc.lat,
              lng: voter.loc.lng,
              icon: voter.party + '.png'
            });
          }
      }
    });  

  });

});


// User Info and Login and Logout Events Handler
Template.userInfo.events ({
  // Login Button Handler
  'click #login': function(){
    Meteor.loginWithFacebook(
      {
        requestPermissions: ['publish_actions', 'email']
      },
      function(err) {
        if( err ) {
          Session.set( "showErrorDialog", true );
          return;
        }
        // Force FB to make a Round Trip to Servers
        FB.getLoginStatus(function(response) {}, true);
      }
    );
    return false;
  }

});


/**
 * Page Template handles whether to show Dialogs or Not
 * @return {[type]} [description]
 */
Template.page.events({
  'click #signout':function(){
    Meteor.logout(  );
  }
});



/**
 * Party Dialog handles Selection of Party
 * @return {[type]} [description]
 */
var openPartyDialog = function() {
  Session.set( "showPartyDialog", true );
};

Template.partyDialog.events ({
  // Set value for Selected Party on .party-btn Click
  'click .party-btn': function( e, t ){  
    var sel = $(e.target).html(  );
    Session.set( "selectedParty", sel );
  },

  // If Not Party Selected Don' Close the Dialog else Close it
  'click #partyDialogDone': function( e, t ){
    var party = Session.get("selectedParty");
    if( party === "None" )
      return;
     Session.set( "showPartyDialog", false )
   }
});

/**
 * Handles visiblity of Change Party button before Vote is Final
 */
Template.selectedParty.events({
  'click #launchPartyDialog': function(){
    Session.set( "showPartyDialog", true );
    return false;
  }
});

/**
 * Post Vote Button triggers a Final Confirmation Dialog
 */
Template.postVote.events({
  'click .btn': function(e,t){
    var coords = Session.get("coordinates");
    var party = Session.get("selectedParty");
    var voter = Session.get("voter");

    if( voter || party === "None" || !coords )
      return false;

    // Show Confirm Dailog
    Session.set( "showConfirmDialog", true );
  }
});

/**
 * Confirmation Dialog transfers control to postOgAction Function 
 * unless Cancel is Pressed. 
 * It also contains Checkbox for Explicit Sharing whose value 
 * is Passed to PostOgAction
 */
Template.confirmDialog.events({
  // If Cancel Pressed, Close the Dialog
  'click #cancel': function(e,t){
    Session.set( "showConfirmDialog", false );
  },
  'click #done': function(e,t){
    // postOgAction( t.find( '#fb-share' ).checked );
    postOgAction();
    Session.set( "showConfirmDialog", false );
  }
})

/**
 * Template shows Alert Boxes for Requirements before Final Vote Insertion
 * and also Success Alert if 
 * @return {[type]} [description]
 */
Template.errors.exists = function(){
  return Session.get( "selectedParty" ) === "None" || !Session.get( "voter" ) || !Session.get( "coordinates" );
};

Template.errors.noPartyMsg = function(){
  return Session.get( "selectedParty" ) === "None" ? "You have not Selected your Party" : "";
};

Template.errors.noCoords = function(){
  return !(Session.get( "coordinates" ) )&& !(Session.get( "selectedParty" ) === "None") ? "Double Click on your Home Location at the Map" : "";
};

Template.errors.noVote = function(){
  return !(Session.get( "voter" )) && Session.get( "coordinates" )   ? "Submit your Vote." : "";
};

/**
 * Closes Error Dailog and Reloads Page
 */
Template.errorDialog.events({
  'click #error-dialog-ok': function(){
    Session.set( "showErrorDialog", false );
    window.location.reload(  );
  }
});

/**
 * Posts Open Graph Action to Facebook for the Vote
 * On Sucess Transfer Control to submitVote
 * @param  {boolean} share Share Explicitly on Facebook
 * @return {[type]}       [description]
 */
var postOgAction = function(share){
  var url = 'http://' + window.location.hostname + '/?party=' + Session.get( "selectedParty" );
  FB.api(
    'me/'+ myApp.fbNamespace +':vote',
    'post',
    {
      'party': url,
      'fb:explicitly_share': 'true' //share
    },
    function(response) {
      // If no Error Save Voter in DB
      if( response && !response.error ){
        submitVote(  );
      }
      else{
        Session.set( "showErrorDialog", true );
      }
    }
  );
};

/**
 * Submits Vote to DB
 * @return {[type]} [description]
 */
var submitVote = function(){
  var coords = Session.get("coordinates");
  var party = Session.get("selectedParty");
  var voter = Session.get("voter");

  if( voter || party === "None" || !coords )
    return false;

  Meteor.call('createVoter', {
    party: party,
    lat: coords.lat,
    lng: coords.lng
  }, function (error, voter) {
    if (! error) {
      Session.set("voter", voter);
      Session.set("showConfirmDialog", false);
      myApp.updateTweetButton();me
      // openInviteDialog();
    }
    else{
      Session.set("showConfirmDialog", false);
      Session.set("showErrorDialog", true);
    }
  });
};